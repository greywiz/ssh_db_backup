#!/bin/bash

cd "${0%/*}"
source backup.conf

# файл задания с параметрами подключения к серверам и бд в формате JSON
# можно передать в параметре при запуске скрипта, либо указать явно в переменной
servers_base=$tasks_dir/$1.json # servers_base=$tasks_dir/bases.json

# формат имени лог-файла для Telegramlog/
log_file="$(date +"%d_%m_%Y_%T").log"

# парсер параметров подключений
servers_list=$(jq -r '. | keys[]' $servers_base)

for server in $servers_list
do

  db_list=$(jq -r ".$(echo '"'$server'"').bases | keys[]" $servers_base)
  db_host=$(jq -r ".$(echo '"'$server'"').credentials.db_host" $servers_base)
  db_port=$(jq -r ".$(echo '"'$server'"').credentials.db_port" $servers_base)
  ssh_port=$(jq -r ".$(echo '"'$server'"').credentials.ssh_port" $servers_base)
  ssh_user=$(jq -r ".$(echo '"'$server'"').credentials.ssh_user" $servers_base)

  # тесты

  # проверка существования директорий для бэкапов и логов
  if ! [ -d $backup_storage/$server ]; then
    mkdir -p $backup_storage/$server
  fi
  if ! [ -d $log_dir ]; then
    mkdir -p $log_dir
  fi

  # проверка доступности подключения по SSH
  echo "$(date +"%d_%m_%Y_%T"): Проверка доступности сервера $server..." | tee -a $log_dir/$log_file
  ssh -p $ssh_port $ssh_user@$server exit
  if [[ $? -eq '0' ]]
  then
    echo "$(date +"%d_%m_%Y_%T"): Сервер $server доступен..." | tee -a $log_dir/$log_file
  else
    echo "$(date +"%d_%m_%Y_%T"): Сервер $server недоступен, невозможно выполнить задание." | tee -a $log_dir/$log_file
    continue
  fi


  # получение параметров подключений к БД
    for db_name in $db_list
    do

      db_user=$(jq -r ".$(echo '"'$server'"').bases.$(echo '"'$db_name'"').db_user" $servers_base)
      db_passwd=$(jq -r ".$(echo '"'$server'"').bases.$(echo '"'$db_name'"').db_passwd" $servers_base)
      file_name="$(echo $db_name)_$(date +"%Y_%m_%d_%H_%M_%S").dump.gz"

      echo "$(date +"%d_%m_%Y_%T"): Резервное копирование базы данных $db_name..." | tee -a $log_dir/$log_file
     
      # команда резервного копирования
      ssh -p $ssh_port $ssh_user@$server "pg_dump 'host=$db_host port=$db_port dbname=$db_name user=$db_user password=$db_passwd' | gzip" > $backup_storage/$server/$file_name && echo "$(date +"%d_%m_%Y_%T"): Задание выполнено. Файл-$(pwd)/$backup_storage/$server/$file_name" | tee -a $log_dir/$log_file || echo "$(date +"%d_%m_%Y_%T"): Задание завершилось с ошибкой." | tee -a $log_dir/$log_file
      sleep 1 &
    
    done

  echo "$(date +"%d_%m_%Y_%T"): Задание для сервера $server выполнено." | tee -a $log_dir/$log_file

done

echo "$(date +"%d_%m_%Y_%T"): Выполнение задания $1 завершено." | tee -a $log_dir/$log_file

# отправка лога выполнения задания в Telegram
if [[ $tg_bot = "true" ]]
then
  curl -F document=@"$log_dir/$log_file" -F caption="Отчет о выполнении задания $1." https://api.telegram.org/bot$tg_bot_key/sendDocument?chat_id=$tg_chat_id &&echo "$(date +"%d_%m_%Y_%T"): Отчет в Telegram." | tee -a $log_dir/$log_file
fi

# сохранение вывода в общий лог
cat $log_dir/$log_file >> $log_dir/$1.backup.log
rm $log_dir/$log_file
